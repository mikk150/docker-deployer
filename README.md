[![license](https://img.shields.io/github/license/herloct/docker-deployer.svg)]()
[![Build Status](https://travis-ci.org/herloct/docker-deployer.svg?branch=master)](https://travis-ci.org/herloct/docker-deployer)

## Supported tags
* 6.8.0
* 6.7.3
* 6.7.2
* 6.7.1
* 6.7.0
* 6.6.0
* 6.5.0
* 6.4.7
* 6.4.6
* 6.4.5
* 6.4.4
* 6.4.3
* 6.4.2
* 6.4.1
* 6.4.0
* 6.3.0
* 6.2.0
* 6.1.0
* 6.0.5
* 6.0.4
* 6.0.3
* 6.0.2
* 6.0.1
* 6.0.0
* 5.1.3
* 5.1.2
* 5.1.1
* 5.1.0
* 5.0.3
* 5.0.2
* 5.0.1
* 5.0.0
* 4.3.4
* 4.3.3
* 4.3.2
* 4.3.1
* 4.3.0
* 4.2.1
* 4.2.0
* 4.1.0
* 4.0.2
* 4.0.1
* 4.0.0
* 3.3.0
* 3.2.1
* 3.2.0
* 3.1.2
* 3.1.1
* 3.1.0
* 3.0.11
* 3.0.10
* 3.0.9
* 3.0.8
* 3.0.7
* 3.0.6
* 3.0.5
* 3.0.4
* 3.0.3
* 3.0.2
* 3.0.1
* 3.0.0

## What is Deployer?

Deployer is a deployment tool written in PHP.

> http://deployer.org/

## How to use this image

Basic usage.

```sh
docker run --rm \
    -v /local/path:/project \
    registry.gitlab.com/mikk150/docker-deployer:tag [<options>]
```

For example, to deploy to default server.

```sh
docker run --rm \
    -v /local/path:/project \
    registry.gitlab.com/mikk150/docker-deployer:tag deploy
```

## Volumes

* `/project`: Your deployment scripts project.
* `/root/.ssh`: Your SSH file(s).
